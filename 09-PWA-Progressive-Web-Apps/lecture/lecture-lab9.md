---
title: Lecture 9
subtitle: Progressive Web Apps

---

<!-- pandoc -s -t revealjs lecture-lab9.md --css=slides.css -V theme=night -o lecture-lab9.html -->


## PWA: Progressive Web Apps

Examples of PWA:

- <https://devdocs.io/>
- <https://www.temu.com/>
- office 365 Outlook

## PWA defined:

"A PWA is a web app developed using specific technologies and standard patterns that allows it to take advantage of both website and native app features, providing greater flexibility to businesses/publishers and better experiences to users. As mobile device usage has grown, delivering a seamless user experience across all devices has become more important than ever."

## PWA defined:

"Like a website, a PWA is mobile-friendly and indexed for SEO. But like apps, PWAs are also installable, available offline, connected to push notifications, and tied to location via GPS. In other words, they’re the best of both worlds."

## Main set of documents

<https://w3c.github.io/manifest/>

## How to make a PWA:

1. add a manifest.json
2. add a service worker
3. adding icons and other accessibility features
4. testing with Lighthouse

## 1. add a manifest.json

A manifest file declares the content of the app, what pages are important, some basic settings and the title(s) of your app.

You will need to make it using JSON, a notation file, the acronym of which stands for JavaScript Object Notation

## 1. add a manifest.json

<https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json>

## 2 add a service worker

A service worker is a special type of script that runs in the browser background. We already have regular js to interact with the front-end pages, service workers are meant to do background jobs. In our case it will manage what code pages we need to save for offline use.

## 2 add a service worker

<https://codeberg.org/UI-Programming-23-24/sample-PWA/src/branch/pages/sw.js>


## 3. adding icons and other accessibility features

As the main purpose of PWA, for us, is to make our game installable on mobile devices, it makes sense that we have to think about the launcher icons that we want the game to use. 

## 3. adding icons and other accessibility features

- <https://codeberg.org/UI-Programming-23-24/sample-PWA/src/branch/main/imgs>

- <https://codeberg.org/UI-Programming-23-24/sample-PWA/src/commit/2bf36b1838e9b29b73f440113b80da23bee0b7d5/manifest.json#L11>

## 4. checking your work and testing with Lighthouse

<https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk>