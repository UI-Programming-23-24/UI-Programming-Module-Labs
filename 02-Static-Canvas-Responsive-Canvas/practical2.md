# Practical 2 - JS canvas

Reminder: be a good citizen, and keep your files well organised.

```
.
├── assets
│   ├── css
│   │   └── screen.css
│   ├── fonts
│   ├── imgs
│   └── js
│       └── script.js
└── index.html

```

After each task is completed, create a git commit, make sure you publish your repo in the `ui-programming-23-24` group and you put a link in the submissions spreadsheet when you're finished with all tasks.

## Task 1

Build a web stack (html, css, js) that includes a `canvas` element. Use Javascript to draw the following items in the canvas itself:

* 4 filled blue squares in each corner of the canvas
* 5 other complex shapes, either as outlines or as filled shapes
* three text elements using 3 different fonts

There are tips below to help you do these tasks.

Comment your javascript code to say what lines are doing different parts of your drawing. `right-click` on your canvas and notice that you can't inspect the content of the canvas the same way you can inspect a web page like wikipedia. 

### learning how to draw in the Canvas:

![this is how the canvas system works](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes/canvas_default_grid.png)

The reference point in `JS Canvas` is top left. 

To draw in the canvas, we'll have to set a variable that'll let us address and program it:

```
// get a handle to the canvas context
const canvas = document.getElementById("the_canvas");

// get 2D context for this canvas
const context = canvas.getContext("2d");

// draw something
context.fillRect(1000, 400, 50, 50);
//fillRect(position x, position y, width, height)
```

Notice that instead of using `let` to set a variable we are using `const` here. While it is technically still a variable, it is a type of variable that can't be reassigned. We're only really going to have one `canvas` so addressing it should be through a `const`. On the other had, `let` variables can be reassigned, at their block level. There is a third type of variable set by using `var` but we try to avoid this one as it is eternally reassignable and not block scoped, so if you re-declare it by mistake, the browser won't throw an error. `var` can be useful if you need a global variable, but it is best practice to not use them and to work at block levels.

See <https://www.w3schools.com/graphics/canvas_drawing.asp> for ideas on how to draw your shapes in the Canvas.

[Here is a link to a cheat-sheet that recaps most of the things you'll need when using the canvas.](https://simon.html5.org/dump/html5-canvas-cheat-sheet.html)

## Task 2

Study the folder ´/responsive-canvas´ and compare the differences between the two canvases specifically regarding the 'mobile responsiveness'. Before delivering your practical, port over the code that you need to make your canvas full of shapes and text responsive. 
