---
title: Lecture 2
subtitle: The canvas
---

<!-- pandoc -s -t revealjs lecture-lab2.md --css=slides.css -V theme=night -o lecture-lab2.html -->

## The Canvas

UI Programming week 2

## Canvas

"Part of HTML5 and allows for dynamic, scriptable rendering of 2D shapes and bitmap images. It is a low level, procedural model that updates a bitmap. HTML5 Canvas also helps in making 2D games. While the HTML5 canvas offers its own 2D drawing API, it also supports the WebGL API to allow 3D rendering with OpenGL."

## Canvas

```
<canvas id="the_canvas" width="1100" height="500">
    <!-- text between the canvas tags won't render unless the browser does not support it -->
    Oops! looks like your browser does not support canvas tag
</canvas>
```

## CanIuse canvas?

Checking browser compatibility is good web-dev practice:

<https://caniuse.com/canvas>

## Responsive design 

Where did 'responsive web design' come from, and what was it motivated by?

- HCI progress over years
- arrival of usable touch screen mobile devices

## Consider these examples

* 2012: <https://web.archive.org/web/20120502103648/http://www.itcarlow.ie/>
* 2022: <https://www.setu.ie/>

## 9 basic principles

The following animated images are extracted from: https://blog.froont.com/9-basic-principles-of-responsive-web-design/

## 2.1

![Responsive vs Adaptive](01_Responsive-vs-Adaptive.gif)

## 2.2

![Flow vs Static](04_Flow-vs-Static-2.gif)

## 2.3

![Relative Units vs Static Units](02_Relative-Units-vs-Static-Units-1.gif)

## 2.4

![Breakpoints](03_With-Breakpoints-vs-Without-Breakpoints-1.gif)

## 2.5

![Maximum and Minimum values](07_Max-width-vx-No-max-width-1.gif)

## 2.6

![Nested Object approach](05_Nested-vs-Not-Nested-1.gif)

## 2.7

![Mobile first or Desktop first](08_Desktop-first-vs-Mobile-first-3.gif)

## 2.8

![Web fonts or System fonts](06_System-fonts-vs-Webfonts-1.gif)

## 2.9

![Bitmap vs Vectors](09_Vectors-vs-Images-1.gif)

## Examples of responsive websites

* <https://www.rte.ie/>
* <https://www.nytimes.com/>
* <https://villa-arson.xyz/edition-experience/#>
* <http://laboratorium.bio/>
* <https://verygoodfilms.co/>

## implementing responsive designs with CSS

```
<link rel="stylesheet" href="main.css" media="screen" />
<link rel="stylesheet" href="paper.css" media="print" />
<link rel="stylesheet" href="tiny.css" media="handheld"/>
```

## Media queries

```
@media screen and (min-width: 1024px) {
    body {font-size: 100%;  }
}
```

