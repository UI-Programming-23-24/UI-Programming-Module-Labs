console.log("hello from script");

function pageLoaded() {

    // get a handle to the canvas context
    const canvas = document.getElementById("the_canvas");

    // get 2D context for this canvas
    const context = canvas.getContext("2d");

    // draw something
    context.fillRect(1000, 400, 50, 50);
    //fillRect(position x, position y, width, height)

    // Complex Paths
    // Filled Triangle
    context.beginPath();
    context.moveTo(200, 120);
    context.lineTo(100, 180);
    context.lineTo(180, 200);
    context.lineTo(200, 120);
    context.stroke();
    
    // Filled Arc
    context.beginPath();
    context.arc(300,200,40,0,Math.PI,true);
    context.stroke();

    //colored stroked rect
    context.strokeStyle = "#FF0000";
    context.strokeRect(20, 20, 150, 100);
    
    // Text
    context.font = "10px";
    context.fillText("Some text over here", 320, 60);
    context.font = '22px monospace';
    context.fillText("Some more text",320,100);
    let image = document.getElementById("image");
    context.drawImage(image, 100, 100, 300, 300);

    let newImg = new Image();
    newImg.src = "assets/img/tesse.gif"

    newImg.onload = function () {
        // Draw image
        context.drawImage(newImg, 300, 300, 50, 50);
    }

}

pageLoaded();