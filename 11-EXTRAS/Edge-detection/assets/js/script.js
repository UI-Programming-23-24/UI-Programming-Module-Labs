const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");
let collided = {
    bottom: false,
    right: false,
    left: false,
    top: false};
let speed = 3;

let image = new Image();
image.src = "assets/img/calcifer.jpg";

let obstacle = new Image();
obstacle.src = "assets/img/castle.png"

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

// Default Player
let player = new GameObject(image, 0, 0, 100, 100);

let castle = new GameObject(obstacle, 200, 200, 100, 100);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
    // Take Input from the Player
    // console.log("Input");
    //console.log(event);
    //console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}

//function collision(r1, r2)
function collision() {
    if (player.x + player.width >= castle.x &&
          player.x <= castle.x + castle.width &&
          player.y + player.height >= castle.y &&
          player.y <= castle.y + castle.height)
    {
      context.fillStyle = "red";  
      const top_diff = castle.y + castle.height - player.y;
      const bottom_diff = player.y + player.height - castle.y;
      const left_diff = castle.x + castle.width - player.x;
      const right_diff = player.x + player.width - castle.x;
  
      const min = Math.min(bottom_diff, top_diff, left_diff, right_diff);
  
      collided =  {
        bottom: bottom_diff == min,
        right: right_diff == min,
        left: left_diff == min,
        top: top_diff == min
      }
    }else{
        collided = {
            bottom: false,
            right: false,
            left: false,
            top: false}
            context.fillStyle = "black";
    }
    //console.log(collided)
    return null;
  }


function update() {
    collision();
    // Check Input
    if (gamerInput.action === "Up") {
        //console.log("Move Up");
        if (collided.top != true){
            player.y -= speed; // Move Player Up
        }
    } else if (gamerInput.action === "Down") {
        //console.log("Move Down");
        if (collided.bottom != true){
            player.y += speed; // Move Player Down
        }
    } else if (gamerInput.action === "Left") {
        //console.log("Move Left");
        if (collided.left != true){
            player.x -= speed; // Move Player Left
        }
    } else if (gamerInput.action === "Right") {
        //console.log("Move Right");
        if (collided.right != true){
            player.x += speed; // Move Player Right
        }    
    }
}

function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log("Draw");
    //console.log(player);
    context.drawImage(player.spritesheet, 
                      player.x,
                      player.y,
                      player.width,
                      player.height);
    context.fillRect(200, 200, 100, 100);
    context.drawImage(castle.spritesheet, castle.x, castle.y, castle.width, castle.height);
    
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
