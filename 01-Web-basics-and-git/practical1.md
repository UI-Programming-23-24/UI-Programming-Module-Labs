# Practical 1

Take example on the `html` file and `assets/` folder in chapter 1 and carry out the steps listed below.

## Part one: starter web project

1. Create a web structure including at least:

- 1 `HTML` file
- linked `CSS` file
- linked `JS` file

You'll need to use lines like these in your `HTML` to connect those files together:

```
<link href="assets/css/screen.css" rel="stylesheet" text="text/css">
<script src="assets/js/script.js"></script>
```

2. Include the following code in your `HTML`:

```
<div id="box">
  <p>click me!</p>
</div>
```

3. Include the following code in your `CSS`:

```
#box{
    width: 200px;
    height: 200px;
    background-color: green;
}

#box p{
    text-align: center;
    padding-top: 20px;
}
```

4. Include the following code in your `JS` file:

```
// below I declare an array of different colours
let colours = ["green", "blue", "red", "orange", "pink"]

let box = document.getElementById("box");
box.addEventListener ("click", changeBoxColour)

function changeBoxColour(){
   let newColour = colours[Math.floor(Math.random() * colours.length)];
   console.log("new box colour is:")
   console.log(newColour);
   box.style.backgroundColor = newColour;
}
```

5. You should have a clickable box in your webpage that changes colour when you click it

6. Read every line of `JS` code you added and comment your `JS` code and write what each line of code is doing. Comments in `JS` are created by starting a line with `//`.

## Part two: publish your starter web project on codeberg.org

Create a Codeberg repository for the basic web code you wrote for Part 1.

You will need a Codeberg.org account for which you will use your student email. You should have received an invitation to join the UI-Programming-23-24 group by email, if you did not, speak to Colm to get an invite.

### Using `gitbash`  and `codeberg.com`

`gitbash` is a tool that allows all operating systems to have similar interactions with the git tool. It is installed on all the machines on campus, you will have to install it on your personal computer if you are using it for this module. You can launch `gitbash` like any other program or application from the start menu, or you can use your file explorer to navigate to the location on your system where you wish to work, right click and use the `Git Bash Here` option to open the shell in that exact location.

**A small list of commands for file system navigation using gitbash:**

`pwd` → print working directory

`ls` → list files and folders in current directory

`cd 'folderName'` → change directory to specific folder (user by adding the folder name as command line argument: `cd Documents/`)

`cd ..` → move working directory one level up (one step backwards in the file tree)

`cp 'originalFileName' 'newFilename'` → creates a copy of originalFile to newFile

`rm 'fileName'` → deletes the specified file (be careful with `rm` it does not move files to the 'Recycle Bin', deletions are ultimate)

`mkdir 'newFolderName'` → makes a new directory (folder) with the specified newFolderName

### Creating a folder for a practical submission (or another project)

- Start by creating a new project on codeberg.org. Sign in to your account using a web browser and click the button saying 'New Repository'. Give the project a title. For UI-Programming practicals, you will be required to reuse the practical title, for example `3-web-forms_data-reception-practical-firstName-lastName`.
- !IMPORTANT! in the Project OWNER field, make sure to select the `ui-programming-23-24` group for the project to appear for the group.
- Intialising the project with a `README.md` can be a good convention but when you are starting off you can un-tick that option, as that will display a helpful guide for the next steps.

### Usual interaction with a git repository

Once your git work folder / project / practical / amazing new website, app or game is intialised, git will be tracking changes as they happen in that specific folder.

Get used to using the `git status` command. (Make sure your terminal / bash session is in the correct location on your file system -- running the 'git status' command in the Documents folder will probably return an error)

`git status` will inform you of what files are currently being tracked by git, what files have changed, what has been deleted and even what has been moved or renamed.

From the onset, you need to tell git what files you want it to track. In a project folder, you might want git to track your code files as they evolve, but other files might not be suited for git. For example, if you are creating a website that will host a video, don't add that video file to git. There are a few reasons for that: firstly, the video file is lightly to be a large file, git is designed to track changes in text and code files not compiled video files. Secondly, the video file is not likely to change in the same way your code will. This video is an external asset; we probably don't need git to keep an eye on it.

`git add index.html` → tells git to start tracking the 'index.html' file
`git add assets/` → tells git to start tracking the 'assets' folder and all its content

Run `git status` to see what git is doing at any time.

Git is a powerful versioning tool that lets you move backwards and forwards in a project history. To enable this, you must get familiar with the practice of commits. Commits or committing is equivalent to creating a 'snapshot' of your project at this current time. Your project will accumulate lots of these different snapshots and these will be the points you can move back to in case you need to revert your code to a previous version.

Once you have added a few files to git with `git add 'file or folder'` run the following command:
`git commit -m "write a message between these quotemaks that describe the latest changes e.g. added a JS function that toggles dark mode on and off"`

Run `git status` again

Your terminal will say something like "Your branch is ahead of 'origin/main' by 1 commit". This specifically means that your **local** project/repository (the files on the computer you are working on) is now ahead of the repository that is on gitlab.com. To synchronise them, you have one last thing left to do:

`git push`

The command above will take your latest commits and synch them with the versions on gitlab.com. This also creates a backup of your work (the repo is stored on gitlab.com, you can always download the latest code from there).

### A video recap of all of the above

Start at 1:01 for CLI recap.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5HLst694D_Y?start=61" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](https://gitlab.com/relearn/Relearn2013/-/raw/master/cheat-sheet/git-scheme.svg)

## Part 3: going further

Quite a few of you completed lab sheet 1 swiftly at our last lab session, this is great!

Here are a few extra tasks and things to look into if you have completed the lab:

0. Make sure you have named your codeberg repository properly before submission:

The naming scheme is: `chapterNumber-chapterTitle-firstName-lastName`

1. Navigate to your practical folder and open your `.html` file in the browser. Hit `f12` or `left-click` and choose `inspect element`. This will open up the web-inspector (it will be slighlty different on each browser, but all browsers have one). Find the `console` in your web-inspector and notice that it is interactive. You can type queries and the console will respond. Type the following line into the console:

```
document.getElementById("box")
```

You should receive a response from the console that details what the browser knows about the element that has an ID of "box". You might have to click on the triangle on the left of the returned item to expand it. Look at the list of things that the browser knows about this single div.

2. Find a way to make the box change size when it is clicked. 

Hints:
- your code will have to be executed when a special event is heard just like before with the colour change and the `.addEventListener()` function

- This line `box.style.backgroundColor = newColour;` in the code would be a good place to try to change things.

3. Add a new element (something similar to our box —you might need some `CSS`) in your `html` document and see if you can program another `eventListener` for it to change. See some ideas below:

```
x.addEventListener("mouseover", myFunction);
x.addEventListener("click", mySecondFunction);
x.addEventListener("mouseout", myThirdFunction);
```

4. Consider these resources, they will be very useful in the module:

- <https://www.w3schools.com/js/js_htmldom_eventlistener.asp>
- <https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model>
