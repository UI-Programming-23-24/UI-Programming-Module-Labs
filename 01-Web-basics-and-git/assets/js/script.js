console.log("Hello World!");

let a = 10;
let b = 20;

if(a > b){
   console.log("a is bigger"); 
}

if(a < b){
   console.log("a is smaller"); 
}

console.log("hello again");

let colours = ["green", "blue", "red", "orange", "pink"]

let box = document.getElementById("box");
box.addEventListener ("click", changeBoxColour)

function changeBoxColour(){
   let newColour = colours[Math.floor(Math.random() * colours.length)];
   console.log("new box colour is:")
   console.log(newColour);
   box.style.backgroundColor = newColour;
}