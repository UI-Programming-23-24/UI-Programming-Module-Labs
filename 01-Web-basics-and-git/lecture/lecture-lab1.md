---
title: Lecture 1
subtitle: Welcome to UI Programming 23-24

---

<!-- pandoc -s -t revealjs lecture-lab1.md --css=slides.css -V theme=night -o lecture-lab1.html -->


### Welcome to the module !

Looking forwards to working with you this semester

## Introductions and information about the module

This is a big module:

* 10 credits
* 8 hours / week
* 9 lab submissions
* 1 final project

## Codeberg repo = home of the module

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs>

## Chapter 1: web basics & Javascript

![](https://clipart.info/images/ccovers/1499794874html5-js-css3-logo-png.png)

## Examples of what we will be making:

- <https://ui-programming-22-23.gitlab.io/final-project-richard-buturla/>

- <https://ui-programming-22-23.gitlab.io/project-anass-doublal/>

- <https://ui-programming-22-23.gitlab.io/project-darragh-mckernan/>

## Starting with html5 and CSS

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Colm ONeill">
        <title>UI Programming 23 24</title>
        <link href="assets/css/screen.css" rel="stylesheet" text="text/css">
    </head>
    <body>
       <h1>Hello world</h1>
       <section>
           This text is placed within a HTML5 tag called <pre><section></pre>
       </section> 
       <script src="assets/js/script.js"></script>
    </body>
</html>
```

## This module is about:

```
<script src="assets/js/script.js"></script>
```

## Folder structure

Be a 'good citizen', organise your files properly

```
.
├── assets
│   ├── css
│   │   └── screen.css
│   ├── fonts
│   ├── imgs
│   └── js
│       └── script.js
└── index.html

```

## Create a HTML, CSS and JS stack

and include:

```
console.log("Hello World!");

let a = 10;
let b = 20;

if(a > b){
   console.log("a is bigger"); 
}

if(a < b){
   console.log("a is smaller"); 
}

console.log("hello again");

```

## Going further

Looking at the console, debugging and the DOM

