const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let image = new Image();
image.src = "assets/img/calcifer.jpg";

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

// Default Player
let player = new GameObject(image, 0, 0, 200, 200);


// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
    // Take Input from the Player
    // console.log("Input");
    console.log(event);
    console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.key);

    if (event.type === "keydown") {
        switch (event.key) {
            case "ArrowLeft": // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case "ArrowUp": // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case "ArrowRight": // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case "ArrowDown": // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}


function update() {
    // console.log("Update");
    // Check Input
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.y -= 1; // Move Player Up
    } else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.y += 1; // Move Player Down
    } else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.x -= 1; // Move Player Left
    } else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.x += 1; // Move Player Right
    }
}

function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log("Draw");
    //console.log(player);
    context.drawImage(player.spritesheet, 
                      player.x,
                      player.y,
                      player.width,
                      player.height);
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
