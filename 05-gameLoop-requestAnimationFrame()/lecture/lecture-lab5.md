---
title: Lecture 5
subtitle: request Animation Frame

---

<!-- pandoc -s -t revealjs lecture-lab5.md --css=slides.css -V theme=night -o lecture-lab5.html -->


## Managing animations

Building towards a solid gameLoop

## setInterval() vs requestAnimationFrame()

`setInterval` can, technically produce high frame-rate animations, but it is not intended for this.

## requestAnimationFrame()

"`requestAnimationFrame` produces higher quality animation completely eliminating flicker and shear that can happen when using `setTimeout` or `setInterval`, and reduce or completely remove frame skips."

## requestAnimationFrame()

The `window.requestAnimationFrame()` method tells the browser that you wish to perform an animation and requests that the browser calls a specified function to update an animation right before the next repaint. The method takes a callback as an argument to be invoked before the repaint. 

## requestAnimationFrame()

This function is one shot, meaning it needs to be called recursively.

```
function smoothAnimation() {
    // animation
    requestAnimationFrame(smoothAnimation)
}
requestAnimationFrame(smoothAnimation)
```

## GameLoop

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/branch/main/05-gameLoop-requestAnimationFrame%28%29/gameloop.png.png>

## GameLoop

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/branch/main/05-gameLoop-requestAnimationFrame%28%29/05B-gameLoop_in_requestAnimationFrame/assets/js/script.js>
