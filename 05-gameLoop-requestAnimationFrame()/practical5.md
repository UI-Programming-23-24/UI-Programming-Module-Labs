# Practical 5

Week 5

## Request Animation Frame + Gameloops

![](gameloop.png.png)

### Task 1:

Study the content of the following folders, run them in the browser, and see how they might help the next tasks. Note that folder `05C` demonstrates the 'requestAnimationFrame()' method and the fuller `gameLoop` is demontrated in `05B`.

```
├── 05A-requestAnimationFrame
├── 05B-gameLoop_in_requestAnimationFrame
├── 05C-joystick-in-gameloop

```

### Task 2:

Adapt and use the GameObject method (https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/commit/8a030952e8c07e2ec4103b68629a301004ba0dea/05-gameLoop-requestAnimationFrame%28%29/05B-gameLoop_in_requestAnimationFrame/assets/js/script.js#L9) to declare at least 3 game objects, one that you move with the Arrow Keys, and two static ones.

### Task 3: 

Adapt and use the GamerInput method (https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/commit/8a030952e8c07e2ec4103b68629a301004ba0dea/05-gameLoop-requestAnimationFrame%28%29/05B-gameLoop_in_requestAnimationFrame/assets/js/script.js#L23) to function with an InputManagement function like you built in lab 4.

### Task 4:

- configure 3 more key-based eventListeners with keys **other than** the keyboard arrows and make one of your canvas characters react based on the input.
 
### Task 5:

- Make the background colour of your web page change when a character you are controlling hits the sides of the canvas. Don't forget to account for the width and height of your character! (See folder 11 for some hints).

### Task 6:

- Check for collision between your character and one of the other static GameObjects you delared in Task 2. When collision is detected, make a sound play. (See folder 11 for some hints).

### Going further:

- Bring in the healthbar from Chapter 4 and make it decrease when your character hits the edge of the canvas, or when collision (task 6) is detected.