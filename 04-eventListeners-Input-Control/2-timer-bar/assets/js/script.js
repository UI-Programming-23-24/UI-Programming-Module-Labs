console.log("hello from script");

const canvas = document.getElementById("the_canvas");
const context = canvas.getContext("2d");

let width = 100;
let height = 20;
let max = 100;
let val = 100;

function drawTimer() {

  // Draw the background
  context.fillStyle = "#000000";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(val / max, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);

  // Decrease the fill
  val = val - 1;
}

setInterval(function(){
  console.log("decreasing val");
  drawTimer();
}, 1000);


