# Practical 4

This practical is all about managing player / user input in the browser—canvas space.

From the lecture content you will have seen how we can use eventListeners to catch keyboard and click inputs. Refer to the lecture content for foundational knowledge.

## Task 1

Study the folders within this chapter...

```
├── 0-inputManagement
├── 1-dpad-example
├── 2-health-bar
├── 3-score-counter
├── 4-joystick
```

... and build a web-page where it is possible to move a 100x100 px `HTML` square using the UP, LEFT, RIGHT and DOWN arrow keys. Do this **without** `canvas` first, using `CSS` and the styling attributes of `JS`.

Add a `H2` title that says: "task 1: html movement" above the space where this square is moving.

For example:

```
character.style.top = parseInt(character.style.top) - 5 + "px";
```

## Task 2

Add a `canvas` below task 1 and give it an outline so it can be seen easily. Using the W, A, S and D keys, make another square move based on keyboard input.

Add a `H2` title that says: "task 2: canvas movement" above the `canvas` where this square is moving.

## Task 3

Import the dpad from folder `1-dpad-example` (this involves importing HTML, CSS and writing JS) and make any modifications you need/want. Connect the dpad key presses to the same movement you created in the `canvas` from task 2.

Notice how the on-screen buttons get visually pressed down, and then spring back up in the example code from folder `1-dpad-example`. Try to bring this into your code for practical 4.

## Task 4

Study the content of folder `2-health-bar` and bring draw a decreasing timer in your `canvas`.

## Task 5

Study the content of folder `3-score-counter` and add a score counter into your `canvas`. Make it increment as your moving square collides with the edge of the canvas.

## Task 6 - going beyond

This is an extra task, which is not mandatory, but good to spend time on if you are finished with all the previous tasks.

Study the content of folder `4-joystick` and see if you understand what is going on. A visual joystick is created when the player clicks in the canvas. This is achieved by importing an external library called nipple.js. You can see details and demos of this joystick here: https://yoannmoi.net/nipplejs/

Try to import the code from this folder into your `canvas` and create a third input method for your square to move in the canvas. You will then have WASD keyboard input, clickable input/movement with the on-screen dPad, and a visual joystick to move the character around.