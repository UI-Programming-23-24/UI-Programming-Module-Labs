# 3 score counter

Similar to the above, giving visual feedback of progress through score is a very useful and playful UI element. Build a score counter into your developing game. Make the visual aesthetic of the score counter in keeping with the rest of the mood of the game. Your score-counter can count amounts of collected items, time spent on a task, amount of kills, etc.

What is the best way to make a score counter? Are you going to use the `.fillText()` function in the canvas or are you going to use a html element that is outside of the canvas?