---
title: Lecture 4
subtitle: Input Management
---

<!-- pandoc -s -t revealjs lecture-lab4.md --css=slides.css -V theme=night -o lecture-lab4.html -->


## Managing player / user input in the canvas

```
//addEventListener(type, listener)
//addEventListener(type, listener, options)
window.addEventListener('keyup', inputManagementFunction);
window.addEventListener('keydown', inputManagementFunction);
```

## Window

```
console.log(window)

Window file:///home/co/OneDrive/2023_24/1_UI-Programming/UI-Programming-23-24-LABS/04-eventListeners-Input-Control/0-inputManagement/index.html
```

## Not calling a function

Note that when the specified event is triggered or heard, we don't exactly call a function, we refer to a `listener`, which can be understood like a function call, so long as we handle the event that is passed.

```
window.addEventListener('keyup', inputManagementFunction);
window.addEventListener('keydown', inputManagementFunction);
```

## Catching the 'event'

```
function ManagementFunction(event) {
    console.log("Key: " + event.key);
    if (event.type === "keydown") {
        switch (event.key) {
            case "ArrowLeft":
                console.log("left arrow pressed");
                moveLeft();
                break;
            case "ArrowUp":
                moveUp();
                break;
            default:
                stopMovement();
        }
    }
}
```

## Example of a movement function

```
let xPos = 0;
let yPos = 0;
let width = 100;
let height = 100;

function moveUp(){    
    context.clearRect(0,0, canvas.width, canvas.height);
    yPos++;
    context.fillRect(xPos, yPos, width, heigth);
}
```

## Building a dpad

```
<table>
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td><button onmousedown="clickDpadYellow()" onmouseup="clickableDpadReleased()" class="button yellow round">Y</button></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><button onmousedown="clickDpadBlue()" onmouseup="clickableDpadReleased()" class="button blue round">X</button></td>
        <td>&nbsp;</td>
        <td><button onmousedown="clickDpadRed()" onmouseup="clickableDpadReleased()" class="button red round">B</button></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><button onmousedown="clickDpadGreen()" onmouseup="clickableDpadReleased()" class="button green round">A</button></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
    </tbody>
</table>
```

## Some `CSS` and `JS` to give visual feedback

```
.pressed{
box-shadow: 0 5px #3D3D3D;
transform: translateY(5px);    
}
```

```
greenButton.classList.add("pressed");
greenButton.classList.remove("pressed");
```

## Thinking together: a visual timer

```
const canvas = document.getElementById("the_canvas");
const context = canvas.getContext("2d");

let width = 100;
let height = 20;
let max = 100;
let val = 100;
```

## A visual timer or a health bar

```
function drawTimer() {

  // Draw the background
  context.fillStyle = "#000000";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(val / max, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);

  // Decrease the fill
  val = val - 1;
}
```

## A visual timer or a health bar

```
setInterval(function(){
  console.log("decreasing val");
  drawTimer();
}, 1000);

```


