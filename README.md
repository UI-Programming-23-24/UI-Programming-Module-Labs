# UI Programming 2023 - 2024 module home

Welcome to the module!

This is the web-home of the module where you will find all the information you need about labs, lectures, submission procedures, projects and some extra notes.

This module is lead by:

* Colm O'Neill
* Fiona Redmond
* Philip Morris

---

### Lectures

https://ui-programming-23-24.codeberg.page/ui-programming-module-labs/

### Module chapters:

This module is structured around 9 chapters, which all have a lab-sheet attached. Each chapter requires a lab submission.

As soon as class lists are finalised, a link will be pasted below to a spreadsheet where you will submit links to your lab submissions.

### Submission spreadsheet:

<https://lite.framacalc.org/ui-prog-submissions-23-24-a3av>

↑ Each submission involves you creating a new git repository, posting your code to this repo and sharing the link.

### grading scheme

#### 0 – 35% Basic

You will obtain a grade in this range if you:

* deliver only parts of the practical
* push a repo to gitlab
* post a link to the CA spreadsheet


#### 35 - 75% Intermediate

You will obtain a grade in this range if you:

* meet all the specs in the briefing
* display a genuine understanding of the themes and practices of the practical
* surpass the practical requirements and display an ability to research and add to the requirements


#### 75 - 100% Advanced

You will obtain a grade in this range if you:

* surpass the practical by demonstrating particular interest in the subjects by adding a lot to the brief, relevant content, advanced styling and genuine interest.