const ctx = document.getElementById('the_canvas').getContext('2d'); 

// canvas width = 1100
// canvas height = 500
// middle width = 1100/2 = 550
// middle height = 500/2 = 250
ctx.translate(550,250);

for (let loop=1; loop <= 28; loop++){
  ctx.strokeStyle = "#FF0000";
  ctx.strokeRect(50, (loop*4), 100, (loop*2));
  ctx.rotate(loop * Math.PI / 180);
}