---
title: Lecture 3
subtitle: Canvas transformations

---

<!-- pandoc -s -t revealjs lecture-lab3.md --css=slides.css -V theme=night -o lecture-lab3.html -->


## The Canvas coordinate system.

Review coordinate system

![](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes/canvas_default_grid.png)

## The Canvas coordinate system

```
//fillRect(position x, position y, width, height)
context.fillRect(1000, 400, 50, 50);
```

## Canvas drawImage()

```
context.drawImage(newImg, 0, 0);
context.drawImage(newImg, 250, 10, 200, 56);
//context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
context.drawImage(newImg, 10, 25, 60, 55, 520, 80, 120, 110);
```

## drawImage()

```
//context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
// img   	Specifies the image, canvas, or video element to use 
// sx    	(Optional.) The x coordinate where to start clipping 	
// sy    	(Optional.) The y coordinate where to start clipping 	
// swidth    	(Optional.) The width of the clipped image 	
// sheight   	(Optional.) The height of the clipped image 	
// x     	The x coordinate where to place the image on the canvas 	
// x     	The x coordinate where to place the image on the canvas 	
// y     	The y coordinate where to place the image on the canvas 	
// width     	(Optional.) The width of the image to use (stretch or reduce the image) 	
// height    	(Optional.) The height of the image to use (stretch or reduce the image)
// check references here https://www.w3schools.com/tags/canvas_drawimage.asp
```

## Canvas transformations

```
context.translate(50, 50);
```

![](Canvas_grid_translate.png)

## Canvas transformations

```
context.rotate(-30);
```

![](Canvas_grid_rotate.png)

## New Objects()

Working with media in the canvas involves working with specific objects.

new Object()

## Image Object, Audio Object

```
//importing an image
let newImg = new Image();
newImg.src = "assets/img/tesse.gif"

//importing a sound
let bounce = new Audio('assets/media/bounce.mp3');
```

## Importing objects does not mean they are used yet:

```
//importing an image
let newImg = new Image();
newImg.src = "assets/img/tesse.gif";
context.drawImage(newImage, 50, 50);

//importing a sound
let bounce = new Audio('assets/media/bounce.mp3');
bounce.play();
```

## About Generative art

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/branch/main/03-Canvas-Transformations/inspiration-generative-drawings/generative-drawings-venice-biennale.md>

## The work of Vera Molnár

<https://yewtu.be/watch?v=8tNESHtfkr0>

## The dot is black

<https://yewtu.be/watch?v=gVSeSV08UEA>
<https://yewtu.be/watch?v=Hns4GVWuf5k>
<https://yewtu.be/watch?v=hdtKeYkyhzs>

