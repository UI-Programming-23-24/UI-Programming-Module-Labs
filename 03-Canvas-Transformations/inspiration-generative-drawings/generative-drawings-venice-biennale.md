# Generative and programmed drawing 
## from Technologies of Enchantment Venice Biennale 2022

![](DSC03220.JPG)

The 1962 exhibition *Arte programmata, Arte cinetica. Opere moltiplicate. Opera aperta*, organised by artist, designer, and polymath Bruno Munari, was an innovative collaboration with the Olivetti corporation —producer of the world's first desktop calculator —at the company's store in Galleria Vittorio Emanuele in Milan. The exhibition featured artists —many associated with Gruppo T and Gruppo N— fascinated by the possibility of using then-nascent computational technologies to produce art. In his introduction to the exhibition's catalogue, philosopher Umberto Eco suggests that these artists should be thought of as "programmers," akin to engineers. Between 1959 and 1963, numerous similar groups emerged across Italy: Rimini's Gruppo V, Gruppo Uno in Rome, Milan's Gruppo MID, and Genoa's Gruppo Tempo 3, among others. In the following decade, museums and galleries in Europe and the US hosted shows of graphics by computer engineers, and Optical and Kinetic artists across the Americas and Europe explored programmed aesthetics. *Nove Tendencije* at the Galerija Suvremene Umjetnosti, Zagreb (1961); *The Responsive Eye* at the Museum of Modern Art, New York (1965); the 33rd International Art Exhibition in Venice (1966); and *Cybernetic Serendipity* at the Institute of Contemporary Art, London (1968) were among several large-scale exhibitions in the 1960s that brought Optical and Programmed Art to a wider audience.

Though the word "computer", until the mid-20th century, referred to the mostly female workers who carried out calculations by hand, the artists presented here were largely marginalised in the overwhelmingly male artistic circles of their time. Conversations around Programmed Art tended either to envision technology as a Powerful new tool for artists's use, or to see the computer as a potential artistic author in its own right; in either case, the fact that new technologies were to purview of men was not in question.

The artists in this presentation bring somatic complexity to "programmed" artistic creation. Whether employing actual industrial materials and technologies —such as Laura Grisi's neon and Plexiglas, Grazia Varisco's megnetic apparatuses, or Nanda Vigo's illuminated glass and mirror— or applying computational logic to work in traditional artistic media— as in Marina Apollonio's optically dynamic reliefs, Dadamaino's hand-painted raster gradients, or the complex mathematical rules governing Lucia Di Luciano's masonite compositions —, each artist included here works on the boundaries between technology and the self. Their work emphasise the optical effect of the viewer's movements, treat screens as skin-like membranes between body and machine, and complicate traditional modes of viewership with the attraction or repulsion of chromatic and luminous surfaces; art is reconceived as a technology of enchantment.



![](DSC03210.JPG)
![](DSC03211.JPG)
![](DSC03212.JPG)
![](DSC03213.JPG)
![](DSC03214.JPG)
![](DSC03215.JPG)
![](DSC03216.JPG)
![](DSC03218.JPG)
![](DSC03221.JPG)
![](DSC03222.JPG)
![](DSC03223.JPG)
![](DSC03224.JPG)
![](DSC03225.JPG)
![](DSC03226.JPG)
![](DSC03227.JPG)
![](DSC03228.JPG)
![](DSC03229.JPG)
![](DSC03230.JPG)
![](DSC03231.JPG)
![](DSC03233.JPG)
![](DSC03236.JPG)
![](DSC03237.JPG)
