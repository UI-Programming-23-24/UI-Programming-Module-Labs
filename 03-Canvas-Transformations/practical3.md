# Lab Sheet 3 - Transformations

In this practical you will be working with:

- Canvas transformations
- Audio content
- Generative Art

# Canvas transformations + audio content

## Task 1:

Find a method where you can repeat a sound automatically three times in a row.

You will need to load a sound file and learn to make it loop.

```
let bounce = new Audio('assets/media/bounce.mp3');
bounce.play();
```

Some examples are left for you in the folder for week 3.

## Task 1:

Display your understanding of canvas transformations by experimenting with generative art. Produce multiple pieces and keep them all in your submission. Start with examples from the following resource:

https://webplatform.github.io/docs/tutorials/canvas/Canvas_tutorial/Transformations/

Make sure your code is up to date with [ES6](https://www.w3schools.com/Js/js_es6.asp) so use `let` and `const` and avoid `var` when possible.

Watch this video <https://www.youtube.com/watch?v=8Uo6zFwSO78> and take inspiration from the content reviewed in the lecture, Vera Molnár, thedotisblack, and any other sources you can find.

If you find yourself limited by the canvas for generative art, consider writing custom drawing functions, and even consider extending your practical by using [p5js](https://scribe.rip/@shvembldr/how-to-make-your-first-generative-art-with-p5-js-3f10afc07de2), (penplot)[https://github.com/mattdesl/penplot] or [canvas sketch](https://github.com/mattdesl/canvas-sketch)