---
title: Lecture 6
subtitle: Spritesheets

---

<!-- pandoc -s -t revealjs lecture-lab6.md --css=slides.css -V theme=night -o lecture-lab6.html -->


## Spritesheets

Heavy reliance on context.drawImage()

## Demo spritesheet

![](../03-spritesheets-in-gameloop/assets/img/Green-16x18-spritesheet.png)

## Demo spritesheet

To make things simple, this demo spritesheet is 48x72px

- each individual image is 16x18px

## drawImage()

https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage

```
//context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
//context.drawImage(imageFile, clippingXpos, clippingYpos, widthOfClip, heightOfClip, XpositionInCanvas, YpositionInCanvas, drawnWidth, drawnHeight);
```

## drawImage

```
//context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
context.drawImage(image, 0, 0, 16, 18, 0, 0, 16, 18);

```

## 16x18 is tiny

Let's scale it up

const scale = 8;

```
//context.drawImage(imageFile, clippingXpos, clippingYpos, widthOfClip, heightOfClip, XpositionInCanvas, YpositionInCanvas, drawnWidth, drawnHeight);
context.drawImage(image, 0, 0, 16, 18, 0, 0, 16 * scale, 18 * scale);
```

## Looking at the demo pages

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/branch/main/06-Spritesheets>

## end of module project spec

<https://codeberg.org/UI-Programming-23-24/UI-Programming-Module-Labs/src/branch/main/10-PROJECT>