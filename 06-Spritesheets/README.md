# Spritesheets

To animate spritesheets based on input we will be heavily reliant on all of the optional argument of the drawImage function

context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);

![](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage/canvas_drawimage.jpg)

* image: ref to Image object, the actual file
* sx: x coordinates of the top left corner of the clipping position
* sy: y coordinates of the top left corner of the clipping position
* sWidth: width of the sub rect (clipping / cropping width)
* sHeight: height of the sub rect 
* dx: x position of the clipped image in canvas
* dy: y position of the clipped image in canvas
* dWidth: sets the width of the clipped rectangle, this allows resizing and transforming
* dHeight: sets the height of the clipped rect, allows resizing, scaling

important to refer to docs here https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage

## sX, sY, sWidth, sHeight

The first four arguments refer to the Selection. `SelectionX, SelectionY, SelectionWidth, SelectionHeight`

In other words, we first clip / crop / select in the spritesheet.

## dX, dY, dWidth, dHeight

The last four arguments refer to the Destination. `DestinationX, DestinationY, DestinationWidth, DestinationHeight`

So these parameters set where the clipped bit of the spritesheet gets shown in the canvas.

## More legible `.drawImage()`

```
context.drawImage(imageVar,
                  
                  SelectionX,
                  SelectionY,
                  SelectionWidth,
                  SelectionHeight,
                  
                  DestinationX,
                  DestinationY,
                  DestinationWidth,
                  DestinationHeight
                 );
 ```
