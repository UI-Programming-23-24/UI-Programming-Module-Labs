# Spritesheets practical 6

## Task 1:

Study the content of folder 01 and get comfortable with the 9 arguments of the `drawImage()` function for spritesheet usage.

## Task 2:

Folder `02-understanding-spritesheets` has an 11 step tutorial in the `script.js` folder. Do each step one by one and build your understanding of the `drawFrame()` function that is proposed here, as well as the `walkLoop[]` array.

## Task 3:

1. build on your code from practical 5 
2. include a spritesheet animation process, see the details in subfolder 02 of this chapter
3. make one of your in-canvas characters use a spritesheet
4. make that spritesheet use a walk/run/movement animation active when gamerInput is registered (keydown event), and disable the animation (or revert to an idle animation) when gamerInput is stopped (keyup event).

