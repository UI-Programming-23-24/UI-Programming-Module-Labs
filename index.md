#### Lecture index

<small>

- <a href="/ui-programming-module-labs/01-Web-basics-and-git/lecture/lecture-lab1.html">lab 01-Web-basics</a>
- <a href="/ui-programming-module-labs/02-Static-Canvas-Responsive-Canvas/lecture/lecture-lab2.html">lab 02-Static-Canvas</a>
- <a href="/ui-programming-module-labs/03-Canvas-Transformations/lecture/lecture-lab3.html">lab 03-Canvas-Transformations</a>
- <a href="/ui-programming-module-labs/04-eventListeners-Input-Control/lecture/lecture-lab4.html">lab 04-eventListeners-Input</a>
- <a href="/ui-programming-module-labs/05-gameLoop-requestAnimationFrame()/lecture/lecture-lab5.html">lab 05-gameLoop-requestAnimationFrame</a>
- <a href="/ui-programming-module-labs/06-Spritesheets/lecture/lecture-lab6.html">lab 06-Spritesheets/lecture</a>
- <a href="/ui-programming-module-labs/07-Web-Storage-API/lecture/lecture-lab7.html">lab 07-Web-Storage</a>
- <a href="/ui-programming-module-labs/08-Public-Git-Pages/lecture/lecture-lab8.html">lab 08-Public-Git</a>
- <a href="/ui-programming-module-labs/09-PWA-Progressive-Web-Apps/lecture/lecture-lab9.html">lab 09-PWA-Progressive</a>

</small>
