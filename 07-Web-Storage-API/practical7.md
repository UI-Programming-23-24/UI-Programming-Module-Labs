# Practical 7

## webStorage API

Make sure to check the documentation online:

* https://www.w3schools.com/JS/js_api_web_storage.asp
* https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API

## Practical 7

Study the example code, specifically observing the localStorage setting and getting, as well as how you can hide and show a div (or other section of html) programatically in Javascript.

Add the following scenario to your existing code-base from practical 6:

- Make the canvas invisible for the users first visit, `element.style.display = "none"` or `element.style.visibililty = "hidden"` are good places to start.
- Make a simple form appear that asks the user to input their name and any other info you might need (refer to the lecture slides for this)
- Store the inputted values into localStorage
- Make the form dissapear using `CSS`
- Make the canvas reappear and let the user play the game. Use localStorage to track their progress (achieving levels, accumulated score, whatever might be relevant to your game)
- Write code that enables the player to close the game tab, re-open the game and be given the option to restore their stored settings, name, score, etc.

## Going further

If you need to store more information than key-value pairs, it is possible to 'encode' data using JSON and the `.stringify()` method. 

See https://www.w3schools.com/js/js_json_stringify.asp for more information on this.
