---
mainfont: IBMPlexSans
---

##### UI Programming -- Project briefing

# Time Travel

### 1. Project Description

Game/Project theme is **Time Travel**

This project you will showcase your newly acquired skills in developing, designing and programming User Interfaces.

Goal is to create a new web-game, contained in a set canvas —your game will be 2 dimensional, taking input, updating display/canvas based on input, checking for collision or danger and listening for input once again.

You will demonstrate creativity and logic when developing this project, you should aim to create an enjoyable and fun interaction for your user, using well-chosen and well thought through UI components.

The game must be based on the theme. Keep in mind that the emphasis of the project is on User Interface, so develop your project wisely, in many cases, small and simple games end up being more fun and enjoyable than complex games.


### 2. Procedure

-   Before using code to develop your game you will spend time ideating and mocking up your game on paper or digital sketching tools. You should have a clear vision of what your game is going to need before you start the development.

-   Prepare a ***paper prototype video*** which is a playthrough of your 1 minute of gameplay.
> examples:
> > https://www.youtube.com/watch?v=yPvv3_SK3Vg

-   Have a strategy for your game visual asset development, either by repurposing existing assets or sourcing / creating original ones.


This project will be based on Web technologies. You are expected to use HTML, CSS and JavaScript for their intended uses:

-   HTML for your front-end structure, forms, buttons and text content
-   CSS for styling your UI, your canvas, making your forms and buttons look and feel nice. CSS enables you to create consistency across your game, making your design look and feel the same across the different interactions is key for good UI and UX

-   JS for handling interactions, eventListening and your overall gameLoop.

### 3. Requirements

Your game will : 

-   Use at least one form (for the user to customise gamerTags and any other preferences/settings that may be relevant to your game (e.g. Sound on/off, dark theme, level selection, game mode, etc))

-   Use a \<canvas>

-   Use a graphical User Interface made up of buttons, triggers, sliders and any relevant interface elements that suit your gameplay

-   Make use of spritesheets for character/player/scene animation

-   Make use of the Web Storage API for consistent data storage between sessions

-   Be comfortably playable on mobile

-   Have a UI that is understandable by all potential users/players. This means making and implementing a design plan for users to be able to find all the interaction features you have built.

-   Be published and be playable through a gitlab.io, bitbucket.io or similar for easy and accessible play.

-   Compliant with PWA Standards meaning it can be installed for offline use through the web browser.

-   Include a submission link to your code repository and your playable game.

### 4. Project timeline

-   **Week 6: 16/10/2023** : project briefing

-   **Week 8: Friday 10/11/2023** : game concept submission, paper and video prototype submission in a project repository that is named FirstName_LastName_StudentNumber-UI-Programming-Module-Project

-   **Week 12: 04/12/2023** : Final Game Submission of Game + Demos starting on Monday 04/12 and finishing on Friday 08/12 at lab/lecture/tutorial hours.   

-   **Your attendance is still mandatory at all module hours during project work periods. Attendance will be taken at each session.**  

### 5. Assessment matrix

This project counts for **50% of the module grade**, and will be scored on 100 points. These 100 points are divided as so:

-   10% Concept design + communication of ideas

-   40% Selection and use of appropriate UI components.

-   30% Compliance with technical requirements (see point 3 Requirements)

-   10% Overall interaction (testing all the parts of the game, from initial form submission to gameplay, scoring, rewarding, game over screen, etc.)

-   10% Overall aesthetics (consistency of the visual style, consideration of the colours and layout, etc)

+-------------------------+---------------------------------+------------------------------+
| **0-35% Basic**         | **35-75% Intermediate**         | **75-100% Advanced**         |
+=========================+=================================+==============================+
| - A selection of the    |  - Game                         | - Game                       |
| basic game              |    implementation               | implementation               |
| requirements has        |    requirement                  | requirement                  |
| been implemented        |    have been                    | have been                    |
| to a basic              |    implemented to               | implemented to               |
| level.                  |    an acceptable                | an advanced                  |
|                         |    level.                       | level.                       |
| - Game                  |                                 |                              |
| implementation          |  - Game                         | - Game                       |
| will achieve            |    implementation               | implementation               |
| minimum                 |    will achieve                 | will not                     |
| functionality.          |    expected                     | contain syntax               |
|                         |    functionality.               | and/or run-time              |
| - Game                  |                                 | errors.                      |
|   implementation        |  - Game                         |                              |
|   may contain some      |    implementation               | - Game                       |
|   syntax and/or         |    will not                     | implementation               |
|   run-time errors.      |    contain syntax               | code will be                 |
|                         |    and/or                       | well commented               |
| - Game                  |    run-time                     | and/or                       |
|   implementation        |    errors.                      | formatted.                   |
|   code will be          |                                 |                              |
|   poorly commented      |  - Game                         | - Game will be               |
|   and/or                |    implementation               | expertly                     |
|   formatted.            |    code will be                 | tested.                      |
|                         |    commented                    |                              |
| - Game                  |    and/or                       | - Game                       |
|   implementation        |    formatted to a               | implementation               |
|   will contain          |    reasonable                   | of code will                 |
|   basic features;       |    level.                       | follow coding                |
|   application will      |                                 | conventions.                 |
|   not be tested         |  - Game will be                 |                              |
|   properly.             |    tested to a                  | - Game                       |
|                         |    reasonable                   | implementation               |
| - Game                  |    degree.                      | will include                 |
|   implementation        |                                 | novel gameplay.              |
|   code will not         |  - Game                         |                              |
|   follow                |    implementation               | - Implementation             |
|   applicable            |    code will                    | utilises                     |
|   coding                |    follow                       | complies with                |
|   conventions.          |    appropriate                  | project                      |
|                         |    coding                       | requirements.                |
| - Game                  |    conventions.                 |                              |
|   implementation        |                                 |                              |
|   will have basic       |  - Game                         |                              |
|   gameplay.             |    implementation               |                              |
|                         |    will have                    |                              |
| - Implementation        |    gameplay as                  |                              |
|   utilises basic        |    specified.                   |                              |
|   use of basic          |                                 |                              |
|   requirements          |  - Implementation               |                              |
|   such as               |    utilises                     |                              |
|   eventListeners()      |    complies with                |                              |
|                         |    the majority                 |                              |
|                         |    project                      |                              |
|                         |    requirements.                |                              |
+-------------------------+---------------------------------+------------------------------+
