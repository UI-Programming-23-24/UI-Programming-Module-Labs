---
title: Lecture 8
subtitle: Git pages

---

<!-- pandoc -s -t revealjs lecture-lab8.md --css=slides.css -V theme=night -o lecture-lab8.html -->


## git pages

## Git pages

The various git remote systems have different approaches to allowing `published pages` on their systems.

## github, gitlab, bitbucket, codeberg, gitea

All these systems use git as a foundation, then add a UI over the top to show the content of remote repositories

## for `codeberg`

On codeberg, we simply need to make a `git branch` called `pages` and push our web content to it.

## Two questions to answer

1. What is codeberg doing to 'serve' the pages?
2. What are git branches?

## 1. Serving pages

## 1.1 Serving pages

The web is made up of tons of computers that 'host' web content. The individual machines that make up the web can be as simple as your home computer, your laptop or even your smartphone.

## 1.2 Serving pages

Because of residential power restraints and network connectivity, it is unfortunately not all that simple to set up your own server at home, but it is possible!

## 1.3 Serving pages

More than likely, the websites you visit are hosted in server farms that consume tons of electricity, not only for keeping all the 'server' machines on, but also for cooling this infrastructure.

## 1.4 Serving pages

![](https://www.wired.com/wp-content/uploads/blogs/wiredenterprise/wp-content/uploads/2012/10/ff_googleinfrastructure2_large.jpg)

## 1.5 Serving the web

In short, what codeberg (or github, gitlab, bitbucket, etc) does to serve pages, is that they give us access to a portion of their web server that is configured (using server software, probably `Apache` or `nginx`) to listen to web requests and serve web pages based on these requests.

## 1.6 Serving and networking

Like a phone call: your computer (the client) makes a request to the host (the server) for various content, using the HTTP protocol.

HTTP = `HyperText Transfer protocol`

## 1.7 Secure serving and networking

The basics of HTTP are still in place today, but we tend to use HTTPS for any secure transactions. The only difference is that transactions between client and server are encrypted.

## 1.8 Web-server defaults

By default, web servers will start by serving pages called `index.html`, so with codeberg, you need that page, or nothing will be served.

## 1.9 Your own web server

You can purchase web-serving products, some are very simple and cheap, others are very complex and expensive. It will depend on the amount of computation you want to do.

`FTP` = File Transfer Protocal

## 1.10 Hosting your own website example

<https://www.blacknight.com/web-hosting/>

## 2. What are git branches?

## 2.1 git and git branches

In Git, a branch is a new/separate version of the main repository.

## 2.2 branches

![](https://wac-cdn.atlassian.com/dam/jcr:a905ddfd-973a-452a-a4ae-f1dd65430027/01%20Git%20branch.svg?cdnVersion=1299)

## 2.3 merging branches in dev

![](https://www.devguide.at/wp-content/uploads/2019/06/branch-1.png)

## 2.4 case for branches

Branches are an excellent way to test an idea, work on a new feature, or show a proposal, while maintaining the advantages of a versioned and backed-up system.

## 2.5 branches in practice

See the practical 8 document for a list of the procedure to create a checkout a branch

## 2.6 branches for codeberg pages

In our case, we will use branches in a slightly unconventional manor. We want our `main` branch to be a mirror of our `pages` branch.

## for codeberg pages

To get codeberg to 'host' your code, all you need to do is to create a `pages` branch, and make sure it has your `index.html` file, plus any other `/assets` your project/site relies on.