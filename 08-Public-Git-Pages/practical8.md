# Practical 8

The task for practical 8 is deliberately short and hopefully simple, to let you focus on your project. It is however essential to complete it, so make sure to complete it.

In short, the task is to take your code from practical 7, and make it accessible online, via codeberg pages. 

* Using the lecture content, follow the steps to make a branch called 'pages' in your practical 8 repo, and push it to codeberg.
* Submit a public link that will look like this `https://USERNAME.codeberg.page[/REPOSITORY][/@BRANCH]` to the submissions spreadsheet.

## Recap of the steps to take:

0. check what branches currently exist in your repo: `git branch -l`
1. if branch 'pages' already exists, proceed to step 3, if not, complete step 2
2. create the new branch called 'pages': `git branch pages`
3. switch to the new branch: `git checkout pages`
4. make sure your 'pages' branch is up to date with your 'main' branch: `git pull origin main` (this pulls the content of origin main into your currently checked-out branch)
5. push your content to the remote: `git push origin pages`

your terminal should have a linke like the following one in the return

```
 * [new branch]      pages -> pages
```

6. check your results by typing the following URL pattern

```https://ui-programming-23-24.codeberg.page/<your-full-repositorys-name>```

for example:

- <https://ui-programming-23-24.codeberg.page/02-Static-Canvas-Responsive-Canvas-Adam-Sharpe/>

- <https://ui-programming-23-24.codeberg.page/ui-programming-module-labs/>